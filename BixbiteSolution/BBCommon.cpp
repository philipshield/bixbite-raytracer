#include "BBCommon.h"

template <class T>
static std::ostream& PrintVector(std::ostream& out, const std::vector<T>& v)
{
	if (v.empty())
		return out;
	out << v[0];
	urep(i, 1, v.size())
		out << " " << v[i];
	return out;
}

namespace std
{
	std::ostream& operator<<(std::ostream& out, const vi& v)
	{
		return PrintVector(out, v);
	}

	std::ostream& operator<<(std::ostream& out, const vu& v)
	{
		return PrintVector(out, v);
	}

	std::ostream& operator<<(std::ostream& out, const std::vector<string>& v)
	{
		return PrintVector(out, v);
	}
};