/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * Common.h contains common includes,
 * typedefs, helperclasses and macros.
 */

#ifndef _BCOMMON_H
#define _BCOMMON_H

/* 
 * Includes
 */

#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cmath>
#include <complex>
#include <limits>
#include <climits>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include <stack>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <cassert>
#include <functional>
#include <chrono>
#include <memory>
#include "tchar.h"
#include <concrt.h>
#include <ppl.h>
#include <thread>

// GLM Includes
#include "GLM\glm.hpp"
#include <GLM\gtx\random.hpp>
#include <GLM\gtc\type_ptr.hpp>
#include <GLM\gtc\matrix_transform.hpp>

// SDL Includes
#include "SDL\SDL.h"

//Windows Includes
#ifdef _WIN32
#include <Windows.h>
#endif

//Debug Includes
#ifdef DEBUG
#include "BBDebug.h"
#include <cstdlib>
#endif

/*
 *  Macros
 */

#define rep(i, a, b) for (int i = (a); i < (b); ++i)
#define rrep(i, b, a) for (int i = (b-1); i >= a; --i)
#define urep(i, a, b) for (size_t i = (a); i < (b); ++i)
#define trav(it, v) for (typeof((v).begin()) it = (v).begin(); it != (v).end(); ++it)
#define log_2(x) (log((double)x)/log(2.0)) //HEREBEDRAGONS: possible conflicts
#define logf_2(x) (logf(float(x))/logf(2.0f))
#define randf() (float(rand())/RAND_MAX)
#define sgn(x) ((x > 0) ? 1 : ((x < 0) ? -1 : 0)) //HEREBEDRAGONS: possible conflicts

#define BEGIN_NAMESPACE(x) namespace x {
#define END_NAMESPACE }

#ifdef _WIN32
#define typeof decltype
#define isnan(x) (_isnan(x))
#define __builtin_expect(exp, c) (exp)
#else 
//other OS
#endif



/*
 *  Typedefs
 */

typedef long long ll;
typedef unsigned long long ull;
typedef std::vector<int> vi;
typedef std::vector<unsigned> vu;
typedef std::vector<float> vf;
typedef std::vector<double> vd;
typedef std::pair<int, int> pii;

typedef std::complex<float> pointf;
typedef std::complex<int> pointi;
#define X(p) ((p).real())
#define Y(p) ((p).imag())
#define endline "\n"

//Bixbite, SDL, GLM

typedef glm::vec3 t_color;



/*
 *  Time 
 */

#ifdef DEBUG
// Time point at which the program was started.
// This does not account for some statically initialized variables.
extern std::chrono::high_resolution_clock::time_point gStart;
#define Now() (std::chrono::high_resolution_clock::now())
#endif



/*
 *  Helper Functions and Classes 
 */

// Get the i'th field of a pair, were i is either 0 or 1.
template<class T>
T& pair_part(std::pair<T, T>& p, int i)
{
	return (i == 0) ? p.first : p.second;
}


namespace std
{
	std::ostream& operator<<(std::ostream& out, const vi& v);
	std::ostream& operator<<(std::ostream& out, const vu& v);
	std::ostream& operator<<(std::ostream& out, const std::vector<string>& v);
};

class misc_error : public std::exception { };
class not_implemented_exception : public std::exception { };

#endif //_BCOMMON_H
