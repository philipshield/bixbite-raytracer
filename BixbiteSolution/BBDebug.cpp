#include "BBCommon.h"
#include "BBDebug.h"

#ifdef DEBUG

static size_t sIndent;
static std::chrono::high_resolution_clock::time_point sLastTime;

PrintLabel::PrintLabel(PrintType type) :
_type(type)
{
}

static std::string PrintTypeName(PrintType type)
{
	switch (type)
	{
	case Trace:
		Display::setTextColor(CColor::DARKGRA);
		return "trace";
	case Info:
		Display::setTextColor(CColor::GREEN);
		return "info";
	case Warning:
		Display::setTextColor(CColor::YELLOW);
		return "WARNING";
	case Error:
		Display::setTextColor(CColor::LIGHTRE);
		return "ERROR";
	default:
		throw not_implemented_exception();
	}
}

namespace std
{
	ostream& operator<<(ostream& out, const PrintLabel& printLabel)
	{
		auto now = std::chrono::high_resolution_clock::now();
		auto diff = now - gStart;
		auto diffLast = (sLastTime.time_since_epoch().count() > 0) ?
			now - sLastTime : std::chrono::high_resolution_clock::duration();
		out << endl << "[" << setw(6) << setprecision(3) << fixed << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() / 1e6 << "]"
			<< "[+" << setw(5) << setprecision(3) << fixed << std::chrono::duration_cast<std::chrono::microseconds>(diffLast).count() / 1e6 << "]"
			<< "[" << setw(7) << PrintTypeName(printLabel._type) << "]"
			<< " " << string(sIndent * INDENT_SIZE, ' ');
		sLastTime = now;
		Display::setTextColor(CColor::WHITE);
		return out;
	}
};

void Display::setTextColor(CColor color) {
	HANDLE cHandle;
	cHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(cHandle, color);
}

#endif

void PrintIndent()
{
#ifdef DEBUG
	++sIndent;
#endif
}

void PrintUnindent()
{
#ifdef DEBUG
	if (sIndent == 0)
		throw misc_error();
	--sIndent;
#endif
}

