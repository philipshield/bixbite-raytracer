#include "Bixbite.h"

// temp globals
#ifdef LIGHTSPHERE
Primitive* lightsphere;
#endif

Bixbite::Bixbite()
:	Initialized(false)
{
	Debug(Trace, "constructing Bixbite object");
}

void Bixbite::Initialize()
{
	Debug(Trace, "constructing Bixbite object");
	
	//+ SETUP & INITIALIZE SDL
	atexit(SDL_Quit);
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		Debug(Error, "could not initialize SDL"); exit(1);
	}
	Uint32 flags = SDL_SWSURFACE | SDL_ASYNCBLIT | SDL_DOUBLEBUF;
	flags |= FULLSCREEN ? SDL_FULLSCREEN : 0;
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, flags);
	if (screen == nullptr)
	{
		Debug(Error, "Could not set video mode: " << string(SDL_GetError())); exit(1);
	}

	//Initialize Scene
	FirstScene.AddCamera(Camera()); //default camera
	
	Camera cornercam;
	cornercam.position = vec3(0.8, -0.8, 0.8);
	cornercam.SetRotation(-2.4, -0.8, 0);
	FirstScene.AddCamera(cornercam);

	Mesh& cornellbox = FirstScene.NewMesh();
	LoadCornellBox(cornellbox);

#ifdef LIGHTSPHERE
	FirstScene.AddLight(UndirectionalLight(LIGHT1_STARTPOS, vec3(1, 1, 1), 10.f, lightsphere));
#endif
	FirstScene.AddLight(UndirectionalLight(LIGHT1_STARTPOS, vec3(1, 1, 1), 10.f));
	FirstScene.AddLight(UndirectionalLight(vec3(-.9, -0.9, -0.9), vec3(1, 1, 1), 6.f));


	//Mesh& secondbox = FirstScene.NewMesh();
	//LoadCornellBox(secondbox);

	//testcode:
	TestCode();

	Initialized = true;
}

int Bixbite::Run()
{
	Debug(Trace, "Initializing Bixbite");
	if (!Initialized)
	{
		Debug(Error, "Bixbite not initialized.");
		return 1;
	}

	// UPDATE/DRAW LOOP
	while (NoQuitMessageSDL())
	{
		//TODO: Parallelize Update/Draw loop
		Update();
		
		if (SDL_MUSTLOCK(screen)) SDL_LockSurface(screen);
	
		cout << time_call([&]{
			std::thread t1([=](){Draw(0, SCREEN_WIDTH / 4, 0, SCREEN_HEIGHT / 2); });
			std::thread t2([=](){Draw(SCREEN_WIDTH / 4, SCREEN_WIDTH / 2, 0, SCREEN_HEIGHT / 2); });
			std::thread t3([=](){Draw(SCREEN_WIDTH / 2, 3 * (SCREEN_WIDTH / 4), 0, SCREEN_HEIGHT / 2); });
			std::thread t4([=](){Draw(3 * (SCREEN_WIDTH / 4), SCREEN_WIDTH, 0, SCREEN_HEIGHT / 2); });
			std::thread t5([=](){Draw(0, SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2, SCREEN_HEIGHT); });
			std::thread t6([=](){Draw(SCREEN_WIDTH / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, SCREEN_HEIGHT); });
			std::thread t7([=](){Draw(SCREEN_WIDTH / 2, 3 * (SCREEN_WIDTH / 4), SCREEN_HEIGHT / 2, SCREEN_HEIGHT); });
			std::thread t8([=](){Draw(3 * (SCREEN_WIDTH / 4), SCREEN_WIDTH, SCREEN_HEIGHT / 2, SCREEN_HEIGHT); });
			t1.join(); t2.join(); t3.join(); t4.join(); t5.join(); t6.join(); t7.join(); t8.join();
		}) << endline;

		if (SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
	}

	SDL_Delay(2000);

	//unload & quit
	SDL_Quit();
	return 0;
}

void Bixbite::Update()
{
	cerr << "."; //used to see the ticks
	Uint8* keystate = SDL_GetKeyState(0);
	float movedist = 0.09f;
	float alpha = 0.06f;

	UndirectionalLight& light = FirstScene.Lights[0];
	Camera& camera = FirstScene.CurrentCamera();
	
	//move light source
	if (keystate[SDLK_LSHIFT])
	{
		int stop = 1;

		if (keystate[SDLK_UP])		{ light.Move(movedist *  WORLD_UP); }		if (keystate[SDLK_DOWN])		{ light.Move(movedist * -WORLD_UP); }		if (keystate[SDLK_RIGHT])	{ light.Move(movedist * WORLD_RIGHT); }		if (keystate[SDLK_LEFT])		{ light.Move(movedist * -WORLD_RIGHT);}
		if (keystate[SDLK_PAGEUP])	{ light.Move(movedist * WORLD_FORWARD); }
		if (keystate[SDLK_PAGEDOWN])	{ light.Move(movedist * -WORLD_FORWARD); }
	}
	else
	{
		//camera 
		if (keystate[SDLK_0]){}
		if (keystate[SDLK_UP]){ camera.Move(movedist*camera.Up()); }		if (keystate[SDLK_DOWN]){ camera.Move(movedist*camera.Down()); }		if (keystate[SDLK_RIGHT]){ camera.Move(movedist*camera.Right()); }		if (keystate[SDLK_LEFT]){ camera.Move(movedist*camera.Left()); }		if (keystate[SDLK_w])    { camera.Rotate(0, alpha, 0); }		if (keystate[SDLK_s])	 { camera.Rotate(0, -alpha, 0); }		if (keystate[SDLK_d])    { camera.Rotate(alpha, 0, 0); }		if (keystate[SDLK_a])	 { camera.Rotate(-alpha, 0, 0); }		if (keystate[SDLK_q])	 { camera.Rotate(0, 0, -alpha); }		if (keystate[SDLK_e])	 { camera.Rotate(0, 0, alpha); }		if (keystate[SDLK_PAGEUP]){ camera.Move(movedist * camera.Forward()); }
		if (keystate[SDLK_PAGEDOWN]){ camera.Move(movedist * camera.Backward()); }
		if (keystate[SDLK_p])
		{
			cout << endline;
			cout << "pos: <" << camera.position.x << ", " << camera.position.y << ", " << camera.position.z << ">" << endline;
			cout << "yaw = " << camera.GetYawAngle() << endline;
			cout << "pitch = " << camera.GetPitchAngle() << endline;
			cout << "roll = " << camera.GetRollAngle() << endline;
		}
		if (keystate[SDLK_1]) { FirstScene.ChangeCamera(1); }
		if (keystate[SDLK_2]) { FirstScene.ChangeCamera(2); }
		if (keystate[SDLK_3]) { FirstScene.ChangeCamera(3); }
	}
}


float r = 1, g = 1, b = 1;
void Bixbite::Draw(int sx, int ex, int sy, int ey)
{
#ifdef GET_DOWN_DISCO_DISCO
	if (SDL_GetTicks() % 100 == 0)
	{
		r = ((double)rand() / (RAND_MAX)) + 1;
		g = ((double)rand() / (RAND_MAX)) + 1;
		b = ((double)rand() / (RAND_MAX)) + 1;
		FirstScene.Lights[0].color = vec3(r, g, b);
	}
#endif

	Ray raythroughpixel;
	vec3 pixel;
	for (int y = sy; y < ey; y++)
	{
		for (int x = sx; x < ex; x++)
		{
			//construct ray through pixel
			FirstScene.CurrentCamera().GetRayTroughPixel(raythroughpixel, y, x, MY, MX);
			
			//trace the ray
			pixel = BACKGROUND_COLOR;
			TraceRay(pixel, FirstScene, raythroughpixel, DEFAULT_TRACING_DEPTH);
			PutPixelSDL(screen, x, y, pixel);
		}
	}
}




void Bixbite::TestCode()
{
	Debug(Trace, "running test code");
	
}



Bixbite::~Bixbite()
{
	//unload world
	Debug(Trace, "calling Bixbite Destructor");

}

