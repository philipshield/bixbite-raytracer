/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains the Bixbite top-level code. 
 * After bixbite is Instantiated and Initialized call Run().
 * Run() is the bixbite main-loop
 *
 */

#ifndef BIXBITE_MAIN_H
#define BIXBITE_MAIN_H

/*
 * Includes, Imports
 */

#include "BBCommon.h"
#include "SDLauxiliary.h"
#include "RayTracer.h"
#include "Scene.h"
#include "Camera.h"
#include "Ray.h"
#include "Light.h"

using namespace glm;
using namespace std;
using namespace std::chrono;

/*
 * Typedefs
 */



/*
 *  Main ``Game��-class. 
 *  Handles SDL setup, game-setup, draw/render-loop, unloading etc
 */
class Bixbite
{
private:

	SDL_Surface* screen = nullptr;

public:

	bool Initialized;	
	Scene FirstScene;

	/*
	 * Bixbite constructor. 
	 * Creates a new bixbite object, using default values
	 */
	Bixbite();

	/*
	 * Initializes SDL, Accelleration Structures, Scenes etc
	 */
	void Initialize();

	/*
	 * Runs the rendering loop
	 */
	int Run();

	/*
	 * Updates the scene
	 */
	void Update();

	/*
	 * Performs raytracing and renders the scene
	 */
	void Draw(int sx, int ex, int sy, int ey);

	/*
	 * Dummy method for testing stuff
	 */
	void TestCode();

	/* 
	 * Bixbite Destructor
	 */
	~Bixbite();
};



#endif
