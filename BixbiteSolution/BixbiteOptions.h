/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 *
 * This file contains global options for bixbite.
 * Preprocessor Defs:
 	algorithm implementations
 
 * SDL:
 	Resolution, sdl opts etc
 
 * World:
 	Reference vector etc
 
 * Raytrace:
 	Limits, helpers etc
 
 * Camera
 	default options
 
 */

#ifndef BIXBITE_OPTIONS_H
#define BIXBITE_OPTIONS_H

/*
* Includes, Imports
*/

#include "BBCommon.h"

using namespace glm;
using namespace std;
using namespace std::chrono;



/* ******************************************* 
 *											 *
 *				BIXBITE OPTIONS              *   
 *											 *
 * ******************************************/

#define SELFSHADOW_BAILOUT
#define TRIANGLE_BACKCULLING
//#define GET_DOWN_DISCO_DISCO
//#define TALLBLOCK
//#define SHORTBLOCK
//#define SPHERE
//#define TWOSPHERES
#define SPHEREANDBOX
//#define LIGHTSPHER

//SDL
const bool FULLSCREEN = false;
const int SCREEN_WIDTH =  1000;
const int SCREEN_HEIGHT = 1000;
const int MX = SCREEN_WIDTH / 2;
const int MY = SCREEN_HEIGHT / 2;

//MATH
const float PI = 3.14159265358979;

//World
const vec3 BACKGROUND_COLOR = vec3(0);
const vec3 ORIGIN = glm::vec3(0, 0, 0); //Origin of R3 coordinate system
const vec3 WORLD_UP(0, -1, 0);
const vec3 WORLD_FORWARD(0, 0, 1);
const vec3 WORLD_RIGHT(1, 0, 0);

//Raytracing
const int DEFAULT_TRACING_DEPTH =  2;
const float RAY_MAX = 50000; //(std::numeric_limits<float>::max)();
const float RAY_EPSILON = 0.000001; //(std::numeric_limits<float>::min)();
const float SHADOW_MINDIST = 0.000001;//(std::numeric_limits<float>::min)();

const vec3 GLOBAL_AMBIENT = vec3(0.1);
const vec3 DEFAULT_DIFFUSE = vec3(0.5);
const vec3 DEFAULT_EMISSIVE = vec3(0);
const vec3 DEFAULT_AMBIENT = vec3(0.1);
const vec3 DEFAULT_SPEC = vec3(0.0001);
const float DEFAULT_SHININESS = 10;

//Light
const vec3 LIGHT1_STARTPOS = vec3(0.8, -0.8, 0.7);
const float DEFAULT_LIGHT_INACC = 0.000; //Sperical Rand
const float DEFAULT_CONST_ATTENUATION_COEFF = PI;
const float DEFUALT_LINEAR_ATTENUATION_COEFF = 1;
const float DEFUALT_QUADRATIC_ATTENUATION_COEFF = 4 * PI;

//Camera
const glm::vec3 CAMERA_defaultposition(0, 0, -std::sqrt(2.5));
const float CAMERA_defaultfocallength = SCREEN_WIDTH/2;


#endif