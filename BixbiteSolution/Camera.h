/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains code for 3D Tranformations, Rotations and Projections
 */

#ifndef BB_CAMERA_H
#define BB_CAMERA_H

/*
 * Forward Declarations
 */

struct Ray;

/*
* Includes
*/

#include "BBCommon.h"
#include "BixbiteOptions.h"
#include "Ray.h"
using namespace std;
using namespace glm;

enum CameraDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	FORWARD,
	BACKWARD
};

/* Represents the view-point in R3. With position and rotation
* great source: http://planning.cs.uiuc.edu/node102.html
*/
struct Camera
{

private:
	const vec3 UP = vec3(0, -1, 0);
	const vec3 RIGHT = vec3(1, 0, 0);
	const vec3 FORWARD = vec3(0, 0, 1);

	mat3 R, Yaw, Pitch, Roll;
	float dyaw, dpitch, droll;

public:

	vec3 position;
	float focallength;

	Camera();
	Camera(vec3 pos, float focallength, float yaw = 0, float pitch = 0, float roll = 0);

	/*
	* Rotates the camera by yaw, pitch, roll
	* (updates matrices accordingly)
	*
	* Default is 0, 0, 0 (no rotation)
	*/
	void Rotate(float yaw = 0, float pitch = 0, float roll = 0);

	/*
	* Moves the camera by specified vector
	*/
	void Move(vec3 v);

	/*
	* Get tracing ray
	*/
	void GetRayTroughPixel(Ray& outray, int y, int x, int my, int mx);

	/*
	* Sets the Total Rotation to yaw, pitch, roll)
	*/
	void SetRotation(float yaw, float pitch, float roll);

	/*
	* Sets yaw and pitch to given viewdirection
	*/
	void SetRotation(vec3 viewdirection);

	/*
	* Sets yaw and pitch to rotate view towards a target point
	*/
	void LookAt(vec3 target);

	/* Sets the Roll Rotation to alpha)
	* (Counterclockwise about z-axis)
	*/
	void SetRoll(float alpha);

	/* Sets the Yaw Rotation to alpha)
	* (Counterclockwise about y-axis)
	*/
	void SetYaw(float beta);

	/* Sets the Pitch Rotation to alpha)
	* (Counterclockwise about x-axis)
	*/
	void SetPitch(float gamma);

	/*
	* Returns the total rotation matrix
	*/
	const mat3& GetRotation() const;

	/* Returns the Roll Rotation Matrix
	* (Counterclockwise about z-axis)
	*/
	const mat3& GetRoll() const;

	/* Returns the Yaw Rotation Matrix
	* (Counterclockwise about y-axis)
	*/
	const mat3& GetYaw() const;

	/* Returns the Pitch Rotation Matrix
	* (Counterclockwise about x-axis)
	*/
	const mat3& GetPitch() const;

	/*
	* Returns the current yaw rotation angle
	*/
	const float GetYawAngle() const;

	/*
	* Returns the current pitch rotation angle
	*/
	const float GetPitchAngle() const;

	/*
	* Returns the current roll rotation angle
	*/
	const float GetRollAngle() const;

	/*
	* Gets forward direction from current view
	*/
	vec3 Forward() const;

	/*
	* Gets backwards direction from current view
	*/
	vec3 Backward() const;

	/*
	* Gets Right direction from current view
	*/
	vec3 Right() const;

	/*
	* Gets Left direction from current view
	*/
	vec3 Left() const;

	/*
	* Gets Down direction from current view
	*/
	vec3 Down() const;

	/*
	* Gets up direction from current view
	*/
	vec3 Up() const;

};


#endif // BB_CAMERA_H