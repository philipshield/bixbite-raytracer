/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains...
*/

#ifndef BB_INTERSECTION_H
#define BB_INTERSECTION_H

/*
* Includes
*/

#include "BBCommon.h"
using namespace std;
using namespace glm;

/*
*  Represents an intersection of a Ray and a triangle
*  //TODO: Generalize intersection to more than only triangles
*/
struct Intersection
{
	vec3 position;
	vec3 N;
	float distance;
	Primitive* triangle;
};


#endif // BB_INTERSECTION_H