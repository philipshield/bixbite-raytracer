/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains...
*/

#ifndef BB_MATERIAL_H
#define BB_MATERIAL_H

/*
* Includes
*/

#include "BBCommon.h"
using namespace std;
using namespace glm;


struct Material
{
	vec3 emissive;
	vec3 ambient;
	vec3 diffuse; 
	vec3 specular;
	float shininess;
	float reflection = 0;

	Material(vec3 dif, vec3 amb, vec3 spec, float shine)
		:
		diffuse(dif),
		ambient(amb),
		specular(spec),
		shininess(shine)
	{}

	Material(vec3 color)
		:
		emissive(DEFAULT_EMISSIVE),
		ambient(color),
		diffuse(color),
		specular(DEFAULT_SPEC),
		shininess(DEFAULT_SHININESS)
	{}

	Material()
		:
		emissive(DEFAULT_EMISSIVE),
		ambient(DEFAULT_AMBIENT),
		diffuse(DEFAULT_DIFFUSE),
		specular(DEFAULT_SPEC),
		shininess(DEFAULT_SHININESS)
	{}

	bool isspecular()
	{
		return specular != vec3(0, 0, 0);
	}

	bool isdiffuse()
	{
		return diffuse != vec3(0, 0, 0);
	}

};



#endif