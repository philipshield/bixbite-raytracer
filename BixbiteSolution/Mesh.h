/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains...
*/

#ifndef BB_MESH_H
#define BB_MESH_H

/*
* Includes
*/

#include "BBCommon.h"
#include "Primitive.h"
using namespace std;
using namespace glm;



/*
* Represents a model as a collection of Triangles
*/
struct Mesh
{
public:
	vector<Primitive*> triangles;
	Mesh();
};


#endif // BB_MESH_H