/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains an abstract (pure virtual) Primitive class, from which meshes are composed.
 * The Primitive interface supports Ray and other primitive intersection algorithms
 *
 * As for now the following hieriarchy is implemented:
 *                       
 *					+-------------+
					|  Primitive  |
					+-------------+
					       |
			--------------------------------------------------------------------------------
			|					 |						|					|
		+-------------+		+-------------+		+-------------+		+-------------+
		|   Triangle  |		|	Sphere	  |		|	(Face)	  |		|(BeizerSurf) |
		+-------------+		+-------------+		+-------------+		+-------------+

 */

#ifndef BB_PRIMITIVE_H
#define BB_PRIMITIVE_H


/*
* Forward Declarations
*/

struct Ray;


/*
* Includes
*/

#include "BBCommon.h"
#include "BixbiteOptions.h"
#include "Material.h"
#include "Ray.h"
using namespace std;
using namespace glm;



/*
 * Abstract Primitive Class.
 */
struct Primitive
{
public:
	
	Material material;

	Primitive(Material material)
		: material(material)
	{}


	virtual bool Intersects(const Ray& ray, float& distance, vec3& intersectionpoint, vec3& norm) const = 0;
	virtual void Move(vec3 v) = 0;

	//TODO: Destructor??	

};


/*
 *
 * Triangle class: Represents a triangular surface
 *
 */
struct Triangle : public Primitive
{

public:

	vec3 v0, v1, v2;
	vec3 N, N1, N2, N3;
	float D, D1, D2, D3;

	Triangle(vec3 v0, vec3 v1, vec3 v2, vec3 color);
	Triangle(vec3 v0, vec3 v1, vec3 v2, Material mat);

	bool Inside(const vec3& point) const;
	void ComputeNormal();

	/*
	* Ray Intersection (implemented by all Primitives)
	*/
	virtual bool Intersects(const Ray& ray, float& distance, vec3& intersectionpoint, vec3& norm) const;
	virtual void Move(vec3 v);
};


/*
 *
 * Sphere class: Represents a sperical surface (mathematical object)
 *
 * See:
 * http://www.scratchapixel.com/lessons/3d-basic-lessons/lesson-7-intersecting-simple-shapes/ray-sphere-intersection/
 */
struct Sphere : public Primitive
{

public:

	vec3 center;
	float radius;

	Sphere(vec3 center, float radius, vec3 color);
	Sphere(vec3 center, float radius, Material mat);

	bool solveQuadratic(const int &a, const int &b, const int &c, int &x0, int &x1);

	/*
	 * Ray Intersection (implemented by all Primitives)
	 */
	virtual bool Intersects(const Ray& ray, float& distance, vec3& intersectionpoint, vec3& norm) const;
	virtual void Move(vec3 v);
};



#endif // BB_PRIMITIVE_H