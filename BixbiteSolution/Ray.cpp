#include "Ray.h"

//BEGIN_NAMESPACE(CGdatastructures)

Ray::Ray()
: origin(vec3(0, 0, 0)), direction(vec3(0, 0, 0))
{ }

Ray::Ray(vec3 origin, vec3 direction)
: origin(origin), direction(direction)
{ }


vec3 Ray::At(float t) const
{
	return origin + direction*t;
}

vec3 Ray::operator*(float t) const
{
	return At(t);
}


//END_NAMESPACE