/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains class definitions for graphics related datastructures used in Bixbite
*
*/

#ifndef BB_RAY_H
#define BB_RAY_H

/*
 * Forward Declarations
 */

struct Camera;
struct Triangle;

/*
* Includes
*/

#include "BBCommon.h"
#include "Camera.h"
#include "Primitive.h"
using namespace std;
using namespace glm;


/*
* Represents a Ray in R3, defined by origin and direction.
* Contains methods for checking intersections with 3D shapes.
*/
struct Ray
{

public:
	vec3 origin;
	vec3 direction;

	Ray();
	Ray(vec3 origin, vec3 direction);
	vec3 At(float t) const;

	vec3 operator*(float t) const;
};


#endif // BB_RAY_H