#include "RayTracer.h"

//temporary globals
#ifdef LIGHTSPHERE
extern Primitive* lightsphere;
#endif

//RAYTRACER CODE:

bool TraceRay(t_color& res, Scene& scene, Ray& ray, int depth, Primitive* ignore)
{
	if (!depth--)
		return false;

	//Render:
	Intersection intersection;
	if (ClosestIntersection(scene, ray, intersection, ignore))
	{
		trav(light, scene.Lights)
		{
			res += DirectLight(scene, intersection, *light, depth);
		}
	}

	return true;
}

bool ClosestIntersection(Scene& scene, Ray ray, Intersection& intersection, Primitive* ignore, UndirectionalLight* light)
{
	intersection.distance = RAY_MAX;
	intersection.triangle = nullptr;

	//TODO: LoD and BSP
	vec3 intersectionpoint, norm;
	float dist = -1;
	urep(i, 0, scene.Meshes[0].triangles.size()) //TODO: primitive-iterator in Scene object instead
	{
#ifdef SELFSHADOW_BAILOUT
		if (scene.Meshes[0].triangles[i] == ignore)
			continue;
#endif

#ifdef LIGHTSPHERE
		if (light != nullptr)
			if (scene.Meshes[0].triangles[i] == light->shape)
				continue;
#endif

		if (!scene.Meshes[0].triangles[i]->Intersects(ray, dist, intersectionpoint, norm))
			continue;

		if (dist < intersection.distance)
		{
			intersection.distance = dist;
			intersection.triangle = scene.Meshes[0].triangles[i];
			intersection.position = intersectionpoint;
			intersection.N = norm;
		}
	}

	if (intersection.triangle == nullptr)
		return false;

	return true;
}

bool inShadow(Scene& scene, Primitive* object, const Intersection& intersection, UndirectionalLight& light)
{
	Ray tolight(intersection.position, normalize(light.position - intersection.position));
	Intersection newintersection;
	if (ClosestIntersection(scene, tolight, newintersection, object)) // TODO: object too
	{
		if (newintersection.distance < length(intersection.position - light.position) && newintersection.distance > SHADOW_MINDIST)
		{
			return true;
		}
	}

	return false;
}

vec3 DirectLight(Scene& scene, const Intersection& intersection, UndirectionalLight& light, int depth)
{
	Primitive* object = intersection.triangle;
	Material* mat = &object->material;
	vec3 N = intersection.N;

	vec3 diffuse(0), specular(0), reflected(0);
	vec3 ambient = mat->ambient * GLOBAL_AMBIENT;
	vec3 emissive = mat->emissive;

	vec3 L((intersection.position - light.position + sphericalRand(light.inaccuracy)));
	if (!inShadow(scene, object, intersection, light)) // not shadow
	{
		//diffuse light
		float difflight = max(dot(N, normalize(L)), 0);
		diffuse = mat->diffuse * (light.color*light.intensity) * difflight;

		bool facing = dot(N, normalize(L)) > 0;
		if (facing)
		{
			//specular
			vec3 V = normalize(intersection.position - scene.CurrentCamera().position);
			vec3 H = vec3(2) * (dot(L, N)*N - L);
			specular = mat->specular * (light.color*light.intensity)
				* (difflight > 0 ? std::pow(max(dot(H, V), 0), mat->shininess) : 0);
			
			//reflection
			if (mat->reflection)
			{
				Ray reflectray(Ray(intersection.position, glm::reflect(V, N) + sphericalRand(0.003f)));
				TraceRay(reflected, scene, reflectray, depth, object);
				reflected *= diffuse*mat->reflection;
			}
		}

	}

	//composite light
	return emissive + ambient + light.Attenuation(length(L))*(diffuse + specular) + reflected;
}







/*
 * SCENE
 */

void LoadCornellBox(Mesh& mesh)
{
	vector<Triangle> triangles;

	// Defines colors:
	Material red(vec3(0.75f, 0.15f, 0.15f));
	Material yellow(vec3(0.75f, 0.75f, 0.15f));
	Material green(vec3(0.15f, 0.75f, 0.15f));
	Material cyan(vec3(0.15f, 0.75f, 0.75f));
	Material blue(vec3(0.15f, 0.15f, 0.75f));
	Material purple(vec3(0.75f, 0.15f, 0.75f));
	Material white(vec3(0.75f, 0.75f, 0.75f));

	//white.reflection = 6.f;
	//purple.reflection = 30.f;
	//yellow.reflection = 30.f;
	//purple.diffuse = vec3(0.01);
	//yellow.diffuse = vec3(0.01);
	//green.reflection = 3.f;
	//cyan.reflection = 0.4;


	triangles.clear();
	triangles.reserve(5 * 2 * 3);

	// ---------------------------------------------------------------------------
	// Room

	float L = 555;			// Length of Cornell Box side.

	vec3 A(L, 0, 0);
	vec3 B(0, 0, 0);
	vec3 C(L, 0, L);
	vec3 D(0, 0, L);

	vec3 E(L, L, 0);
	vec3 F(0, L, 0);
	vec3 G(L, L, L);
	vec3 H(0, L, L);

	// Floor:
	triangles.push_back(Triangle(C, B, A, green));
	triangles.push_back(Triangle(C, D, B, green));

	// Left wall			
	triangles.push_back(Triangle(A, E, C, purple));
	triangles.push_back(Triangle(C, E, G, purple));

	// Right wall		
	triangles.push_back(Triangle(F, B, D, yellow));
	triangles.push_back(Triangle(H, F, D, yellow));

	// Ceiling			
	triangles.push_back(Triangle(E, F, G, cyan));
	triangles.push_back(Triangle(F, H, G, cyan));

	// Back wall	
	triangles.push_back(Triangle(G, D, C, white));
	triangles.push_back(Triangle(G, H, D, white));

	// ---------------------------------------------------------------------------
	// Short block
#ifdef SHORTBLOCK
	A = vec3(290, 0, 114);
	B = vec3(130, 0, 65);
	C = vec3(240, 0, 272);
	D = vec3(82, 0, 225);

	E = vec3(290, 165, 114);
	F = vec3(130, 165, 65);
	G = vec3(240, 165, 272);
	H = vec3(82, 165, 225);

	// Front
	triangles.push_back(Triangle(E, B, A, red));
	triangles.push_back(Triangle(E, F, B, red));

	// Front
	triangles.push_back(Triangle(F, D, B, red));
	triangles.push_back(Triangle(F, H, D, red));

	// BACK
	triangles.push_back(Triangle(H, C, D, red));
	triangles.push_back(Triangle(H, G, C, red));

	// LEFT
	triangles.push_back(Triangle(G, E, C, red));
	triangles.push_back(Triangle(E, A, C, red));

	// TOP
	triangles.push_back(Triangle(G, F, E, red));
	triangles.push_back(Triangle(G, H, F, red));
	//*/
#endif

	// ---------------------------------------------------------------------------
	// Tall block
#ifdef TALLBLOCK
	A = vec3(423, 0, 247);
	B = vec3(265, 0, 296);
	C = vec3(472, 0, 406);
	D = vec3(314, 0, 456);

	E = vec3(423, 330, 247);
	F = vec3(265, 330, 296);
	G = vec3(472, 330, 406);
	H = vec3(314, 330, 456);

	// Front
	triangles.push_back(Triangle(E, B, A, blue));
	triangles.push_back(Triangle(E, F, B, blue));

	// Front
	triangles.push_back(Triangle(F, D, B, blue));
	triangles.push_back(Triangle(F, H, D, blue));

	// BACK
	triangles.push_back(Triangle(H, C, D, blue));
	triangles.push_back(Triangle(H, G, C, blue));

	// LEFT
	triangles.push_back(Triangle(G, E, C, blue));
	triangles.push_back(Triangle(E, A, C, blue));

	// TOP
	triangles.push_back(Triangle(G, F, E, blue));
	triangles.push_back(Triangle(G, H, F, blue));
#endif




#ifdef SPHERE
	// sphere
	Material spheremat(vec3(0.5, 0.4, 0.7));
	spheremat.shininess = 70;
	spheremat.reflection = 1.f;
	mesh.triangles.push_back(new Sphere(vec3(0, 0.6, 0), 0.4f, spheremat));
#endif



#ifdef SPHEREANDBOX
	A = vec3(290, 0, 114);
	B = vec3(130, 0, 65);
	C = vec3(240, 0, 272);
	D = vec3(82, 0, 225);

	E = vec3(290, 165, 114);
	F = vec3(130, 165, 65);
	G = vec3(240, 165, 272);
	H = vec3(82, 165, 225);

	// Front
	triangles.push_back(Triangle(E, B, A, red));
	triangles.push_back(Triangle(E, F, B, red));

	// Front
	triangles.push_back(Triangle(F, D, B, red));
	triangles.push_back(Triangle(F, H, D, red));

	// BACK
	triangles.push_back(Triangle(H, C, D, red));
	triangles.push_back(Triangle(H, G, C, red));

	// LEFT
	triangles.push_back(Triangle(G, E, C, red));
	triangles.push_back(Triangle(E, A, C, red));

	// TOP
	triangles.push_back(Triangle(G, F, E, red));
	triangles.push_back(Triangle(G, H, F, red));
	//*/


#endif






// ----------------------------------------------
	// Scale to the volume [-1,1]^3

	for (size_t i = 0; i<triangles.size(); ++i)
	{
		triangles[i].v0 *= 2 / L;
		triangles[i].v1 *= 2 / L;
		triangles[i].v2 *= 2 / L;

		triangles[i].v0 -= vec3(1, 1, 1);
		triangles[i].v1 -= vec3(1, 1, 1);
		triangles[i].v2 -= vec3(1, 1, 1);

		triangles[i].v0.x *= -1;
		triangles[i].v1.x *= -1;
		triangles[i].v2.x *= -1;

		triangles[i].v0.y *= -1;
		triangles[i].v1.y *= -1;
		triangles[i].v2.y *= -1;

		triangles[i].ComputeNormal();
	}
	//copy to mesh
	mesh.triangles.clear();
	trav(it, triangles)
		mesh.triangles.push_back(new Triangle(*it));



#ifdef TWOSPHERES
	// sphere
	Material spheremat1(vec3(0.5, 0.4, 0.7));
	spheremat1.specular = vec3(0.6);
	spheremat1.shininess = 70;
	spheremat1.reflection = 1.f;
	spheremat1.diffuse = vec3(0.14);
	mesh.triangles.push_back(new Sphere(vec3(-.3f, 0.6f, 0.3f), 0.4f, spheremat1));
	Material spheremat2(vec3(0.5, 0.4, 0.2));
	spheremat2.shininess = 70;
	spheremat2.reflection = 1.f;
	spheremat2.diffuse = vec3(0.14);
	spheremat2.specular = vec3(0.6);
	mesh.triangles.push_back(new Sphere(vec3(.4f, 0.8f, -0.1f), 0.2f, spheremat2));

#endif




#ifdef SPHEREANDBOX 
	// sphere
	Material spheremat1(vec3(0.5, 0.4, 0.7));
	//spheremat1.shininess = 70;
	spheremat1.reflection = 1;
	spheremat1.specular = vec3(0.1);
	spheremat1.diffuse = vec3(0.14);
	mesh.triangles.push_back(new Sphere(vec3(-.3f, 0.6f, 0.3f), 0.4f, spheremat1));
#endif

#ifdef LIGHTSPHERE
	Material spheremat2(vec3(1));
	spheremat2.emissive = vec3(1.0);
	lightsphere = new Sphere(LIGHT1_STARTPOS, 0.06f, spheremat2);
	mesh.triangles.push_back(lightsphere);
#endif

}