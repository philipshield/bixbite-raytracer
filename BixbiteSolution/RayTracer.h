/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains the main Ray-Tracing algorithms in Bixbite,
 * including lightning
 */

#ifndef BIXBITE_RAYTRACER_H
#define BIXBITE_RAYTRACER_H

/*
 * Includes, Imports
 */

#include "BBCommon.h"
#include "Scene.h"
#include "Camera.h"
#include "Mesh.h"
#include "Intersection.h"
#include "Primitive.h"
#include "Ray.h"
#include "Light.h"

using namespace glm;
using namespace std;


/*
* Performs a Raytrace on a scene through a specified pixel
*/
bool TraceRay(
	t_color& res, 
	Scene& scene, 
	Ray& ray, 
	int depth, 
	Primitive* ignore = nullptr
	);

/*
* Calculates the closest intersection of any triangle in Mesh from <start, dir>
*/
bool ClosestIntersection(
	Scene& scene,
	Ray ray,
	Intersection& intersection,
	Primitive* ignore = nullptr,
	UndirectionalLight* light = nullptr
	);


bool inShadow(Scene& scene, const Intersection& intersection, UndirectionalLight& light);

/*
* Calculates the direct light at the surface of an intersection
*/
vec3 DirectLight(Scene& scene, const Intersection& intersection, UndirectionalLight& light, int depth);

/*
* Cornell Box
*/
void LoadCornellBox(Mesh& mesh);



#endif // BIXBITE_RAYTRACER_H