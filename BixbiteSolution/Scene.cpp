#include "Scene.h"


Scene::Scene()
: currentcamera(0)
{

}

Camera& Scene::CurrentCamera()
{
	return Cameras[currentcamera];
}

void Scene::AddCamera(Camera camera)
{
	Cameras.push_back(camera);
}

bool Scene::ChangeCamera(int i)
{
	if (Cameras.size() == 1)
	{
		Debug(Warning, "Only 1 camera");
		return false;
	}

	
	i--; //1-indiced input

	if ( i == currentcamera)
	{
		Debug(Info, "Already on camera " << i+1);
		return false;
	}
	if (i < 0 || i > Cameras.size())
	{
		Debug(Warning, "could not change to camera " << i + 1);
		return false;
	}

	Debug(Info, "Changing to Camera " << i + 1);
	currentcamera = i;
	return true;
}

void Scene::AddLight(UndirectionalLight light)
{
	Lights.push_back(light);
}

void Scene::AddMesh(Mesh mesh)
{
	Meshes.push_back(mesh);
}

Mesh& Scene::NewMesh()
{
	Meshes.push_back(Mesh());
	return Meshes[Meshes.size() - 1];
}

Scene::~Scene()
{
}
