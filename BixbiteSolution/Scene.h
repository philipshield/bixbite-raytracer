/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains code for representing a Scene. 
 * A Scene consists of Lights, Meshes and a camera.
 
 * (TODO:) It also provides an API for quering the vertices 
   with accelleration structures
 */

#ifndef BIXBITE_SCENE_H
#define BIXBITE_SCENE_H

/*
* Includes, Imports
*/

#include "BBCommon.h"
#include "Camera.h"
#include "Light.h"
#include "Mesh.h"

using namespace glm;
using namespace std;
using namespace std::chrono;

class Scene
{
	int currentcamera;
public:

	vector<Camera> Cameras;
	vector<Mesh> Meshes;
	vector<UndirectionalLight> Lights;

	Scene();



	void AddCamera(Camera camera);
	bool ChangeCamera(int i);
	Camera& CurrentCamera();

	void AddLight(UndirectionalLight light);
	
	Mesh& NewMesh();
	void AddMesh(Mesh mesh);


	~Scene();
};

#endif