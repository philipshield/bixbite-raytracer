#include "Primitive.h"



Sphere::Sphere(vec3 center, float radius, vec3 color)
: Primitive(color), center(center), radius(radius)
{
}

Sphere::Sphere(vec3 center, float radius, Material mat)
: Primitive(mat), center(center), radius(radius)
{
}

bool Sphere::solveQuadratic(const int &a, const int &b, const int &c, int &x0, int &x1)
{
	int discr = b * b - 4 * a * c;
	if (discr < 0) return false;
	else if (discr == 0) x0 = x1 = -0.5 * b / a;
	else {
		int q = (b > 0) ?
			-0.5 * (b + sqrt(discr)) :
			-0.5 * (b - sqrt(discr));
		x0 = q / a;
		x1 = c / q;
	}
	if (x0 > x1) std::swap(x0, x1);
	return true;
}


/*
* Ray Intersection (implemented by all Primitives)
*/
bool Sphere::Intersects(const Ray& ray, float& distance, vec3& intersectionpoint, vec3& norm) const
{
	vec3 temp = ray.origin - center;

	float a = dot(ray.direction, ray.direction);
	float b = 2 * dot(ray.direction, temp);
	float c = dot(temp, temp) - radius*radius;

	float discriminant = b*b - 4 * a*c;

	// first check to see if ray intersects sphere
	if (discriminant > 0)
	{
		discriminant = std::sqrt(discriminant);
		float t = (-b - discriminant) / (2 * a);

		// now check for valid interval
		if (t < 0)
			t = (-b + discriminant) / (2 * a);
		if (t < 0 || t > RAY_MAX)
			return false;

		// we have a valid hit
		distance = t;
		intersectionpoint =  ray.At(t);
		norm = normalize(center - intersectionpoint);
		return true;
	}
	return false;
}


void Sphere::Move(vec3 v)
{
	center += v;
}