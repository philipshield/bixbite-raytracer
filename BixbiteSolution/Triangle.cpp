#include "Primitive.h"

Triangle::Triangle(vec3 v0, vec3 v1, vec3 v2, vec3 color)
: Primitive(Material(color)), v0(v0), v1(v1), v2(v2)
{
	ComputeNormal();
}

Triangle::Triangle(vec3 v0, vec3 v1, vec3 v2, Material mat)
: Primitive(mat), v0(v0), v1(v1), v2(v2)
{}

bool Triangle::Inside(const vec3& point) const
{
	return !(dot(N1, point) + D1 < 0.0f ||
			 dot(N2, point) + D2 < 0.0f || 
			 dot(N3, point) + D3 < 0.0f);
}

void Triangle::ComputeNormal()
{
	N = normalize(cross(v1 - v0, v2 - v0));
	D = -dot(N, v0);

	// "bounds":
	N1 = normalize(cross(N, v1 - v0));
	D1 = -dot(N1, v0);

	N2 = normalize(cross(N, v2 - v1));
	D2 = -dot(N2, v1);

	N3 = normalize(cross(N, v0 - v2));
	D3 = -dot(N3, v2);
}


bool Triangle::Intersects(const Ray& ray, float& distance, vec3& intersectionpoint, vec3& norm) const
{
	float nrdot = -dot(N, ray.direction);

#ifdef TRIANGLE_BACKCULLING
	if (nrdot > 0)
		return false;
#else
	if (nrdot == 0)
		return false;
#endif

	distance = (dot(N, ray.origin) + D) / nrdot;
	if (distance < RAY_MAX) // && distance >= 0)
	{
		intersectionpoint = ray.origin + ray.direction*distance;
		norm = N;
		return Inside(intersectionpoint);
	}
	
	return false;
}

void Triangle::Move(vec3 v)
{
	v0 += v;
	v1 += v;
	v2 += v;
}