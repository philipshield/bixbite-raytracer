/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains...
*/

#ifndef BB_TRIANGLE_H
#define BB_TRIANGLE_H


/*
 * Forward Declarations
 */

struct Ray;

/*
* Includes
*/

#include "BBCommon.h"
#include "Ray.h"
#include "Material.h"
using namespace std;
using namespace glm;

#endif