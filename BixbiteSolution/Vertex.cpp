#include "Vertex.h"

Vertex::Vertex(float x, float y, float z, float w)
: vec4(x, y, z, w)
{}

Vertex::Vertex(vec4 other)
: vec4(other)
{}

Vertex::Vertex(vec3 vec, float w)
	: vec4(vec.x, vec.y, vec.z, w)
{}

vec3 Vertex::ToVec3()
{
	if (w != 0)
	{
		return vec3(x / w, y / w, z / w);
	}
	else
	{
		return normalize(vec3(x, y, z));
	}
}

ostream& operator<< (std::ostream& stream, const vec3& vec)
{
	stream << "<" << vec.x << "," << vec.y << "," << vec.z << ">";
	return stream;
}

ostream& operator<< (std::ostream& stream, const Vertex& vertex)
{
	if (vertex.w != 0) //point in R3
	{
		stream << "<" << vertex.x / vertex.w << "," << vertex.y / vertex.w << "," << vertex.z / vertex.w << ",  (" << vertex.w << ")>";
	}
	else //direction in R3 (normalized)
	{
		stream << ((vec3)vertex);
	}
	return stream;
}
