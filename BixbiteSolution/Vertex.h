/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains...
*/

#ifndef BB_VERTEX_H
#define BB_VERTEX_H

/*
* Includes
*/

#include "BBCommon.h"

using namespace std;
using namespace glm;

/*
* Represents a (Homogenous) Vertex, <x, y, z, w> in the Bixbite Engine.
*
* Inherits from vec4 but uses vec3 internally as well, updating with each operation.
* At any time, a vec3 <x/w, y/w, z/w> or a vec4 <x, y, z, w> can be obtained.
*
* if w >= 1 then the vertex is interpreted as a point, otherwise as a direction
*/
class Vertex : public vec4
{

public:
	Vertex(float x = 0.f, float y = 0.f, float z = 0.f, float w = 1);
	Vertex(vec3 vec, float w);
	Vertex(vec4 other);
	vec3 ToVec3();

	friend std::ostream& operator<< (std::ostream& stream, const Vertex& vertex);
};

#endif // BB_VERTEX_H