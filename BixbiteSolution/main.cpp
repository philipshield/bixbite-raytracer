/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains the main entry point
 *
 */


/*
 * Forward Declarations 
 */

void Update();
void Draw();
void ClosestIntersectionTest();

/*
 * Includes
 */

#include "Bixbite.h"
#include "BBCommon.h"
#include "SDLauxiliary.h"
#include "CGdatastructures.h"

using namespace glm;
using namespace std;
using namespace std::chrono;

/*
 *  Globals
 */

#ifdef DEBUG
high_resolution_clock::time_point gStart(high_resolution_clock::now());
#endif

/*
 * Main Entry Point
 */

Bixbite engine;

int main(int argc, char* argv[])
{
	//Run Raytracer:
	engine.Initialize();

	int exitstatus = engine.Run();

	if (exitstatus != 0)
	{
		cout << "exiting with error code " << exitstatus << "." << endline;
	}
	else
	{
		cout << "exiting.." << endline;
	}

	return 0;
}
